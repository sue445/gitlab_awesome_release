#!/bin/bash -xe

bundle config set --local path 'vendor/bundle/'
bundle config set --local clean 'true'

ruby --version
gem --version
bundle --version

bundle install --jobs $(nproc)
bundle update
